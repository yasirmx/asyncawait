﻿using System.Threading;

namespace Forms.Tpl
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows.Forms;


    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private async void btnSearchDirectory_Click(object sender, EventArgs e)
        {

            string[] files;

            var task = Task.Run(() =>
            {
                files = Directory.GetFiles(@"d:\");
                foreach (var result in files)
                {
                    if (lstDirectory.InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate { lstDirectory.Items.Add(result); }));

                    }
                    else
                    {
                        lstDirectory.Items.Add(result);
                    }
                }

            });

            task.ContinueWith(t =>
            {

                MessageBox.Show("Completed", "Async Method Complete");
            });

            task.ConfigureAwait(true) //continue on UI thread
                .GetAwaiter() //see if something completed or not
                .OnCompleted(() => MessageBox.Show("Completed", "UI completed"));//schedules a continuation

            Async();

            GrabResult();

            try
            {
                await ThrowExceptionAsync(); //notice method signature is async void
            }
            catch (Exception exception)
            {
                btnSearchDirectory.Text = "Exception Occured {0} ";
            }

            var response = await GetStringAsyncTask(); //get return value

            //Conclusion: Anyc/Await run on UI thread

            var response2 = await GetStringAsyncTask().ConfigureAwait(false);
            //code below will not run on UI thread
            btnSearchDirectory.Text = "Error";

            //UI thread here
            await Task.Delay(1000);
            //UI thread here
            await Task.Delay(2000);//create continuations
        }

        private async void Async()
        {
            //async tells the compiler that the method will be run asynchronously and we have a continuation

            await Task.Run(() => Thread.Sleep(2000)); //schedule a continuation after asynchronous operation is completed

            btnSearchDirectory.Text = "Await completed";
        }

        private async void GrabResult()
        {
            var result = await Task.Run(
                () =>
                {
                    return "This returns a string";
                });
            MessageBox.Show(string.Format("Result returned is : {0}", result));
        }

        private async Task ThrowExceptionAsync() //async Task does not return a value
        { 
            throw new UnauthorizedAccessException("Unauthorized access"); //State machine sets exception to the Task in order to track exception
            //need to check task for any exception
            var result = await Task.Run(() =>
            {
                return "Unauthorized access";
            });

            MessageBox.Show(result);
        }

        private async Task<string> GetStringAsyncTask()
        {
            return await Task.Run(() => { return "hello world"; });
        }

        private async Task<string> RunAllAsync()
        {
            //all three fired at the same time
            var loginTask = Task.Run(() =>
            {
                Thread.Sleep(2000);
                return "Sleep";
            });

            var lotTask = Task.Delay(2000);

            var purchaseTask = Task.Delay(1000);

            //return when all three tasks complete
            await Task.WhenAll(loginTask, lotTask, purchaseTask);

            return loginTask.Result;
        }
    }
}
